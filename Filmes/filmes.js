//filmes = [year, title, studios, producers, winner]
var filmes = [
["1980", "Can't Stop the Music", ["Associated Film Distribution"], ["Allan Carr"], "yes"],
["1980", "Cruising", ["Lorimar Productions", "United Artists"], ["Jerry Weintraub"], ""],
["1980", "The Formula", ["MGM", "United Artists"], ["Steve Shagan"], ""],
["1980", "Friday the 13th", ["Paramount Pictures"], ["Sean S. Cunningham"], ""],
["1980", "The Nude Bomb", ["Universal Studios"], ["Jennings Lang"], ""],
["1980", "The Jazz Singer", ["Associated Film Distribution"], ["Jerry Leider"], ""],
["1980", "Raise the Titanic", ["Associated Film Distribution"], ["William Frye"], ""],
["1980", "Saturn 3", ["Associated Film Distribution"], ["Stanley Donen"], ""],
["1980", "Windows", ["United Artists"], ["Mike Lobell"], ""],
["1980", "Xanadu", ["Universal Studios"], ["Lawrence Gordon"], ""],
["1981", "Mommie Dearest", ["Paramount Pictures"], ["Frank Yablans"], "yes"],
["1981", "Endless Love", ["Universal Studios", "PolyGram"], ["Dyson Lovell"], ""],
["1981", "Heaven's Gate", ["United Artists"], ["Joann Carelli"], ""],
["1981", "The Legend of the Lone Ranger", ["Universal Studios", "Associated Film Distribution"], ["Walter Coblenz"], ""],
["1981", "Tarzan", ["MGM", "United Artists"], ["John Derek"], ""],
["1982", "Inchon", ["MGM"], ["Mitsuharu Ishii"], "yes"],
["1982", "Annie", ["Columbia Pictures"], ["Ray Stark"], ""],
["1982", "Butterfly", ["Analysis Film Releasing"], ["Matt Cimber"], ""],
["1982", "Megaforce", ["20th Century Fox"], ["Albert S. Ruddy"], ""],
["1982", "The Pirate Movie", ["20th Century Fox"], ["David Joseph"], ""],
["1983", "The Lonely Lady", ["Universal Studios"], ["Robert R. Weston"], "yes"],
["1983", "Hercules", ["MGM", "United Artists", "Cannon Films"], ["Yoram Globus", "Menahem Golan"], ""],
["1983", "Jaws 3-D", ["Universal Studios"], ["Rupert Hitzig"], ""],
["1983", "Stroker Ace", ["Warner Bros.", "Universal Studios"], ["Hank Moonjean"], ""],
["1983", "Two of a Kind", ["20th Century Fox"], ["Roger M. Rothstein", "Joe Wizan"], ""],
["1984", "Bolero", ["Cannon Films"], ["Bo Derek"], "yes"],
["1984", "Cannonball Run II", ["Warner Bros."], ["Albert S. Ruddy"], ""],
["1984", "Rhinestone", ["20th Century Fox"], ["Marvin Worth", "Howard Smith"], ""],
["1984", "Sheena", ["Columbia Pictures"], ["Paul Aratow"], ""],
["1984", "Where the Boys Are '84", ["TriStar Pictures"], ["Allan Carr"], ""],
["1985", "Rambo: First Blood Part II", ["Columbia Pictures"], ["Buzz Feitshans"], "yes"],
["1985", "Fever Pitch", ["MGM", "United Artists"], ["Freddie Fields"], ""],
["1985", "Revolution", ["Warner Bros."], ["Irwin Winkler"], ""],
["1985", "Rocky IV", ["MGM", "United Artists"], ["Irwin Winkler", "Robert Chartoff"], ""],
["1985", "Year of the Dragon", ["MGM", "United Artists"], ["Dino De Laurentiis"], ""],
["1986", "Howard the Duck", ["Universal Studios"], ["Gloria Katz"], "yes"],
["1986", "Under the Cherry Moon", ["Warner Bros."], ["Bob Cavallo", "Joe Ruffalo", "Steve Fargnoli"], "yes"],
["1986", "Blue City", ["Paramount Pictures"], ["William L. Hayward", "Walter Hill"], ""],
["1986", "Cobra", ["Warner Bros.", "Cannon Films"], ["Yoram Globus", "Menahem Golan"], ""],
["1986", "Shanghai Surprise", ["MGM"], ["John Kohn"], ""],
["1987", "Leonard Part 6", ["Columbia Pictures"], ["Bill Cosby"], "yes"],
["1987", "Ishtar", ["Columbia Pictures"], ["Warren Beatty"], ""],
["1987", "Jaws: The Revenge", ["Universal Studios"], ["Joseph Sargent"], ""],
["1987", "Tough Guys Don't Dance", ["Cannon Films"], ["Yoram Globus", "Menahem Golan"], ""],
["1987", "Who's That Girl", ["Warner Bros."], ["Rosilyn Heller", "Bernard Williams"], ""],
["1988", "Cocktail", ["Touchstone Pictures"], ["Ted Field", "Robert W. Cort"], "yes"],
["1988", "Caddyshack II", ["Warner Bros."], ["Neil Canton", "Jon Peters", "Peter Guber"], ""],
["1988", "Hot to Trot", ["Warner Bros."], ["Steve Tisch"], ""],
["1988", "Mac and Me", ["Orion Pictures"], ["R. J. Louis"], ""],
["1988", "Rambo III", ["TriStar Pictures", "Carolco Pictures"], ["Buzz Feitshans"], ""],
["1989", "Star Trek V: The Final Frontier", ["Paramount Pictures"], ["Harve Bennett"], "yes"],
["1989", "The Karate Kid Part III", ["Columbia Pictures"], ["Jerry Weintraub"], ""],
["1989", "Lock Up", ["TriStar Pictures", "Carolco Pictures"], ["Charles Gordon", "Lawrence Gordon"], ""],
["1989", "Road House", ["United Artists"], ["Joel Silver"], ""],
["1989", "Speed Zone", ["Orion Pictures"], ["Murray Shostack"], ""],
["1990", "The Adventures of Ford Fairlane", ["20th Century Fox"], ["Steven Perry", "Joel Silver"], "yes"],
["1990", "Ghosts Can't Do It", ["Triumph Releasing"], ["Bo Derek"], "yes"],
["1990", "The Bonfire of the Vanities", ["Warner Bros."], ["Brian De Palma"], ""],
["1990", "Graffiti Bridge", ["Warner Bros."], ["Randy Phillips", "Craig Rice"], ""],
["1990", "Rocky V", ["United Artists"], ["Robert Chartoff", "Irwin Winkler"], ""],
["1991", "Hudson Hawk", ["TriStar Pictures"], ["Joel Silver"], "yes"],
["1991", "Cool as Ice", ["Universal Studios"], ["Carolyn Pfeiffer", "Lionel Wingram"], ""],
["1991", "Dice Rules", ["Seven Arts Productions"], ["Loucas George"], ""],
["1991", "Nothing but Trouble", ["Warner Bros."], ["Lester Berman", "Robert K. Weiss"], ""],
["1991", "Return to the Blue Lagoon", ["Columbia Pictures"], ["William A. Graham"], ""],
["1992", "Shining Through", ["20th Century Fox"], ["Carol Baum", "Howard Rosenman"], "yes"],
["1992", "The Bodyguard", ["Warner Bros."], ["Kevin Costner", "Lawrence Kasdan", "Jim Wilson"], ""],
["1992", "Christopher Columbus: The Discovery", ["Warner Bros."], ["Alexander Salkind", "Ilya Salkind"], ""],
["1992", "Final Analysis", ["20th Century Fox"], ["Paul Junger Witt", "Charles Roven", "Tony Thomas"], ""],
["1992", "Newsies", ["Walt Disney Pictures"], ["Michael Finnell"], ""],
["1993", "Indecent Proposal", ["Paramount Pictures"], ["Sherry Lansing"], "yes"],
["1993", "Body of Evidence", ["MGM", "United Artists"], ["Dino De Laurentiis"], ""],
["1993", "Cliffhanger", ["TriStar Pictures", "Carolco Pictures"], ["Renny Harlin", "Alan Marshall"], ""],
["1993", "Last Action Hero", ["Columbia Pictures"], ["John McTiernan", "Stephen J. Roth"], ""],
["1993", "Sliver", ["Paramount Pictures"], ["Robert Evans"], ""],
["1994", "Color of Night", ["Hollywood Pictures"], ["Buzz Feitshans", "David Matalon"], "yes"],
["1994", "North", ["Columbia Pictures", "Castle Rock Entertainment"], ["Rob Reiner", "Alan Zweibel"], ""],
["1994", "On Deadly Ground", ["Warner Bros."], ["A. Kitman Ho", "Julius R. Nasso", "Steven Seagal"], ""],
["1994", "The Specialist", ["Warner Bros."], ["Jerry Weintraub"], ""],
["1994", "Wyatt Earp", ["Warner Bros."], ["Kevin Costner", "Lawrence Kasdan", "Jim Wilson"], ""],
["1995", "Showgirls", ["MGM", "United Artists"], ["Charles Evans", "Alan Marshall"], "yes"],
["1995", "Congo", ["Paramount Pictures"], ["Kathleen Kennedy", "Sam Mercer"], ""],
["1995", "It's Pat", ["Touchstone Pictures"], ["Charles B. Wessler"], ""],
["1995", "The Scarlet Letter", ["Hollywood Pictures"], ["Roland JoffÇ¸", "Andrew G. Vajna"], ""],
["1995", "Waterworld", ["Universal Studios"], ["Kevin Costner", "John Davis", "Charles Gordon", "Lawrence Gordon"], ""],
["1996", "Striptease", ["Columbia Pictures", "Castle Rock Entertainment"], ["Andrew Bergman", "Mike Lobell"], "yes"],
["1996", "Barb Wire", ["PolyGram Filmed Entertainment", "Gramercy Pictures"], ["Todd Moyer", "Mike Richardson", "Brad Wyman"], ""],
["1996", "Ed", ["Universal Studios"], ["Rosalie Swedlin"], ""],
["1996", "The Island of Dr. Moreau", ["New Line Cinema"], ["Edward R. Pressman"], ""],
["1996", "The Stupids", ["New Line Cinema", "Savoy Pictures"], ["Leslie Belzberg"], ""],
["1997", "The Postman", ["Warner Bros."], ["Kevin Costner", "Steve Tisch", "Jim Wilson"], "yes"],
["1997", "Anaconda", ["Columbia Pictures"], ["Verna Harrah", "Carole Little", "Leonard Rabinowitz"], ""],
["1997", "Batman & Robin", ["Warner Bros."], ["Peter MacGregor-Scott"], ""],
["1997", "Fire Down Below", ["Warner Bros."], ["Julius R. Nasso", "Steven Seagal"], ""],
["1997", "Speed 2: Cruise Control", ["20th Century Fox"], ["Jan de Bont", "Steve Perry", "Michael Peyser"], ""],
["1998", "An Alan Smithee Film: Burn Hollywood Burn", ["Hollywood Pictures"], ["Ben Myron", "Joe Eszterhas"], "yes"],
["1998", "Armageddon", ["Touchstone Pictures"], ["Michael Bay", "Jerry Bruckheimer"], ""],
["1998", "The Avengers", ["Warner Bros."], ["Jerry Weintraub"], ""],
["1998", "Godzilla", ["TriStar Pictures"], ["Roland Emmerich", "Dean Devlin"], ""],
["1998", "Spice World", ["Columbia Pictures"], ["Uri Fruchtan", "Mark L. Rosen", "Barnaby Thompson"], ""],
["1999", "Wild Wild West", ["Warner Bros."], ["Jon Peters", "Barry Sonnenfeld"], "yes"],
["1999", "Big Daddy", ["Columbia Pictures"], ["Sidney Ganis", "Jack Giarraputo"], ""],
["1999", "The Blair Witch Project", ["Artisan Entertainment"], ["Robin Cowie", "Gregg Hale"], ""],
["1999", "The Haunting", ["DreamWorks"], ["Susan Arthur", "Donna Roth", "Colin Wilson"], ""],
["1999", "Star Wars: Episode I ƒ?? The Phantom Menace", ["20th Century Fox"], ["Rick McCallum", "George Lucas"], ""],
["2000", "Battlefield Earth", ["Warner Bros.", "Franchise Pictures"], ["Jonathan D. Krane", "Elie Samaha", "John Travolta"], "yes"],
["2000", "Book of Shadows: Blair Witch 2", ["Artisan Entertainment"], ["Bill Carraro"], ""],
["2000", "The Flintstones in Viva Rock Vegas", ["Universal Studios"], ["Bruce Cohen"], ""],
["2000", "Little Nicky", ["New Line Cinema"], ["Jack Giarraputo", "Robert Simonds"], ""],
["2000", "The Next Best Thing", ["Paramount Pictures"], ["Leslie Dixon", "Linne Radmin", "Tom Rosenberg"], ""],
["2001", "Freddy Got Fingered", ["20th Century Fox"], ["Larry Brezner", "Howard Lapides", "Lauren Lloyd"], "yes"],
["2001", "Driven", ["Warner Bros.", "Franchise Pictures"], ["Renny Harlin", "Elie Samaha", "Sylvester Stallone"], ""],
["2001", "Glitter", ["20th Century Fox", "Columbia Pictures"], ["Laurence Mark",  "E. Bennett Walsh"], ""],
["2001", "Pearl Harbor", ["Touchstone Pictures"], ["Michael Bay", "Jerry Bruckheimer"], ""],
["2001", "3000 Miles to Graceland", ["Warner Bros.", "Franchise Pictures"], ["Demian Lichtenstein", "Eric Manes", "Elie Samaha", "Richard Spero", "Andrew Stevens"], ""],
["2002", "Swept Away", ["Screen Gems"], ["Matthew Vaughn"], "yes"],
["2002", "The Adventures of Pluto Nash", ["Warner Bros."], ["Martin Bregman", "Michael Scott Bregman", "Louis A. Stroller"], ""],
["2002", "Crossroads", ["Paramount Pictures"], ["Ann Carli"], ""],
["2002", "Pinocchio", ["Miramax Films"], ["Gianluigi Braschi", "Nicoletta Braschi", "Elda Ferri"], ""],
["2002", "Star Wars: Episode II ƒ?? Attack of the Clones", ["20th Century Fox"], ["Rick McCallum", "George Lucas"], ""],
["2003", "Gigli", ["Columbia Pictures", "Revolution Studios"], ["Martin Brest", "Casey Silver"], "yes"],
["2003", "The Cat in the Hat", ["Universal Studios", "DreamWorks"], ["Brian Grazer"], ""],
["2003", "Charlie's Angels: Full Throttle", ["Columbia Pictures"], ["Drew Barrymore", "Leonard Goldberg", "Nancy Juvonen"], ""],
["2003", "From Justin to Kelly", ["20th Century Fox"], ["John Steven Agoglia"], ""],
["2003", "The Real Cancun", ["New Line Cinema"], ["Mary-Ellis Bunim", "Jonathan Murray", "Jamie Schutz", "Rick de Oliveira"], ""],
["2004", "Catwoman", ["Warner Bros."], ["Denise Di Novi", "Edward McDonnell"], "yes"],
["2004", "Alexander", ["Warner Bros."], ["Moritz Borman", "Jon Kilik", "Thomas Schuhly", "Iain Smith"], ""],
["2004", "Superbabies: Baby Geniuses 2", ["Triumph Films"], ["Steven Paul"], ""],
["2004", "Surviving Christmas", ["DreamWorks"], ["Betty Thomas", "Jenno Topping"], ""],
["2004", "White Chicks", ["Columbia Pictures", "Revolution Studios"], ["Rick Alvarez", "Lee R. Mayes", "Keenen Ivory Wayans", "Marlon Wayans","Shawn Wayans"], ""],
["2005", "Dirty Love", ["First Look Pictures"], ["John Mallory Asher", "BJ Davis", "Rod Hamilton", "Kimberley Kates", "Michael Manasseri", "Jenny McCarthy", "Trent Walford"], "yes"],
["2005", "Deuce Bigalow: European Gigolo", ["Columbia Pictures"], ["Adam Sandler","Rob Schneider"], ""],
["2005", "The Dukes of Hazzard", ["Warner Bros.", "Village Roadshow"], ["Bill Gerber"], ""],
["2005", "House of Wax", ["Warner Bros.", "Village Roadshow"], ["Susan Levin", "Joel Silver","Robert Zemeckis"], ""],
["2005", "Son of the Mask", ["New Line Cinema"], ["Erica Huggins", "Scott Kroopf"], ""],
["2006", "Basic Instinct 2", ["MGM", "C2 Pictures"], ["Mario Kassar", "Joel B. Michaels", "Andrew G. Vajna"], "yes"],
["2006", "BloodRayne", ["Romar Entertainment"], ["Uwe Boll", "Dan Clarke", "Wolfgang Herrold"], ""],
["2006", "Lady in the Water", ["Warner Bros."], ["Sam Mercer", "Jose L. Rodriguez", "M. Night Shyamalan"], ""],
["2006", "Little Man", ["Columbia Pictures", "Revolution Studios"], ["Rick Alvares", "Lee Mays", "Marlon Wayans", "Shawn Wayans"], ""],
["2006", "The Wicker Man", ["Warner Bros."], ["Nicolas Cage", "Randall Emmett", "Norm Golightly", "Avi Lerner", "Joanne Sellar"], ""],
["2007", "I Know Who Killed Me", ["TriStar Pictures"], ["David Grace", "Frank Mancuso Jr."], ""],
["2007", "Bratz", ["Lionsgate"], ["Avi Arad", "Isaac Larian","Steven Paul"], ""],
["2007", "Daddy Day Camp", ["TriStar Pictures", "Revolution Studios"], ["Matt Berenson", "John Davis", "Wyck Godfrey"], ""],
["2007", "I Now Pronounce You Chuck & Larry", ["Universal Studios"], ["Adam Sandler", "Tom Shadyac"], ""],
["2007", "Norbit", ["DreamWorks"], ["John Davis", "Eddie Murphy", "Michael Tollin"], ""],
["2008", "The Love Guru", ["Paramount Pictures"], ["Gary Barber", "Michael DeLuca", "Mike Myers"], "yes"],
["2008", "Disaster Movie and Meet the Spartans", ["Lionsgate", "20th Century Fox"], ["Jason Friedberg", "Peter Safran", "Aaron Seltzer"], ""],
["2008", "The Happening", ["20th Century Fox"], ["Barry Mendel", "Sam Mercer", "M. Night Shyamalan"], ""],
["2008", "The Hottie & the Nottie", ["Regent Releasing"], ["Hadeel Reda"], ""],
["2008", "In the Name of the King", ["Boll KG", "Brightlight Pictures"], ["Uwe Boll", "Dan Clarke", "Wolfgang Herrold", "Shawn Williamson"], ""],
["2009", "Transformers: Revenge of the Fallen", ["Paramount Pictures", "DreamWorks", "Hasbro"], ["Lorenzo di Bonaventura", "Ian Bryce", "Tom DeSanto", "Don Murphy"], "yes"],
["2009", "All About Steve", ["20th Century Fox"], ["Sandra Bullock", "Mary McLaglen"], ""],
["2009", "G.I. Joe: The Rise of Cobra", ["Paramount Pictures", "Hasbro"], ["Lorenzo di Bonaventura", "Bob Ducsay", "Brian Goldner"], ""],
["2009", "Land of the Lost", ["Universal Studios"], ["Sid and Marty Krofft", "Jimmy Miller"], ""],
["2009", "Old Dogs", ["Walt Disney Pictures"], ["Peter Abrams", "Robert Levy", "Andrew Panay"], ""],
["2010", "The Last Airbender", ["Paramount Pictures", "Nickelodeon Movies"], ["Frank Marshall", "Kathleen Kennedy", "Sam Mercer", "M. Night Shyamalan"], "yes"],
["2010", "The Bounty Hunter", ["Columbia Pictures"], ["Neal H. Moritz"], ""],
["2010", "Sex and the City 2", ["New Line Cinema", "HBO Films", "Village Roadshow Pictures"], ["Michael Patrick King", "John Melfi", "Sarah Jessica Parker", "Darren Star"], ""],
["2010", "The Twilight Saga: Eclipse", ["Summit Entertainment"], ["Wyck Godfrey", "Karen Rosenfelt"], ""],
["2010", "Vampires Suck", ["20th Century Fox"], ["Jason Friedberg", "Peter Safran", "Aaron Seltzer"], ""],
["2011", "Jack and Jill", ["Columbia Pictures"], ["Todd Garner", "Jack Giarraputo", "Adam Sandler"], "yes"],
["2011", "Bucky Larson: Born to Be a Star", ["Columbia Pictures"], ["Barry Bernardi", "Allen Covert", "David Dorfman", "Jack Giarraputo"], ""],
["2011", "New Year's Eve", ["Warner Bros.", "New Line Cinema"], ["Mike Karz", "Garry Marshall", "Wayne Allan Rice"], ""],
["2011", "Transformers: Dark of the Moon", ["Paramount Pictures"], ["Lorenzo di Bonaventura", "Ian Bryce", "Tom DeSanto", "Don Murphy"], ""],
["2011", "The Twilight Saga: Breaking Dawn ƒ?? Part 1", ["Summit Entertainment"], ["Wyck Godfrey", "Stephenie Meyer", "Karen Rosenfelt"], ""],
["2012", "The Twilight Saga: Breaking Dawn ƒ?? Part 2", ["Summit Entertainment"], ["Wyck Godfrey", "Stephenie Meyer", "Karen Rosenfelt"], "yes"],
["2012", "Battleship", ["Universal Studios"], ["Sarah Aubrey", "Peter Berg", "Brian Goldner", "Duncan Henderson", "Bennett Schneir", "Scott Stuber"], ""],
["2012", "The Oogieloves in the Big Balloon Adventure", ["Lionsgate Films", "Romar Entertainment", "Kenn Viselman Presents"], ["Gayle Dickie", "Kenn Viselman"], ""],
["2012", "That's My Boy", ["Columbia Pictures"], ["Allen Covert", "Jack Giarraputo", "Heather Parry", "Adam Sandler"], ""],
["2012", "A Thousand Words", ["Paramount Pictures", "DreamWorks"], ["Nicolas Cage", "Alain Chabat", "Stephanie Danan", "Norman Golightly", "Brian Robbins", "Sharla Sumpter Bridgett"], ""],
["2013", "Movie 43", ["Relativity Media"], ["Peter Farrelly", "Ryan Kavanaugh", "John Penotti", "Charles B. Wessler"], "yes"],
["2013", "After Earth", ["Columbia Pictures"], ["James Lassiter", "Caleeb Pinkett", "Jada Pinkett Smith", "M. Night Shyamalan", "Will Smith", "Jaden Smith"], ""],
["2013", "Grown Ups 2", ["Columbia Pictures"], ["Jack Giarraputo", "Adam Sandler"], ""],
["2013", "The Lone Ranger", ["Walt Disney Pictures"], ["Jerry Bruckheimer", "Gore Verbinski"], ""],
["2013", "A Madea Christmas", ["Lionsgate"], ["Ozzie Areu", "Matt Moore", "Tyler Perry"], ""],
["2014", "Saving Christmas", ["Samuel Goldwyn Films"], ["Darren Doane", "Raphi Henley", "Amanda Rosser", "David Shannon"], "yes"],
["2014", "Left Behind", ["Freestyle Releasing", "Entertainment One"], ["Michael Walker", "Paul LaLonde"], ""],
["2014", "The Legend of Hercules", ["Summit Entertainment"], ["Boaz Davidson", "Renny Harlin", "Danny Lerner", "Les Weldon"], ""],
["2014", "Teenage Mutant Ninja Turtles", ["Paramount Pictures", "Nickelodeon Movies", "Platinum Dunes"], ["Michael Bay", "Ian Bryce", "Andrew Form", "Bradley Fuller", "Scott Mednick", "Galen Walker"], ""],
["2014", "Transformers: Age of Extinction", ["Paramount Pictures"], ["Ian Bryce", "Tom DeSanto", "Lorenzo di Bonaventura", "Don Murphy"], ""],
["2015", "Fantastic Four", ["20th Century Fox"], ["Simon Kinberg", "Matthew Vaughn", "Hutch Parker", "Robert Kulzer", "Gregory Goodman"], "yes"],
["2015", "Fifty Shades of Grey", ["Universal Pictures", "Focus Features"], ["Michael De Luca", "Dana Brunetti", "E. L. James"], "yes"],
["2015", "Jupiter Ascending", ["Warner Bros."], ["Grant Hill", "The Wachowskis"], ""],
["2015", "Paul Blart: Mall Cop 2", ["Columbia Pictures"], ["Todd Garner", "Kevin James", "Adam Sandler"], ""],
["2015", "Pixels", ["Columbia Pictures"], ["Adam Sandler", "Chris Columbus", "Mark Radcliffe", "Allen Covert"], ""],
["2016", "Hillary's America: The Secret History of the Democratic Party", ["Quality Flix"], ["Gerald R. Molen"], "yes"],
["2016", "Batman v Superman: Dawn of Justice", ["Warner Bros."], ["Charles Roven", "Deborah Snyder"], ""],
["2016", "Dirty Grandpa", ["Lionsgate"], ["Bill Block", "Michael Simkin", "Jason Barrett", "Barry Josephson"], ""],
["2016", "Gods of Egypt", ["Summit Entertainment"], ["Basil Iwanyk", "Alex Proyas"], ""],
["2016", "Independence Day: Resurgence", ["20th Century Fox"], ["Dean Devlin", "Harald Kloser", "Roland Emmerich"], ""],
["2016", "Zoolander 2", ["Paramount Pictures"], ["Stuart Cornfeld", "Scott Rudin", "Ben Stiller", "Clayton Townsend"], ""],
["2017", "The Emoji Movie", ["Columbia Pictures"], ["Michelle Raimo Kouyate"], "yes"],
["2017", "Baywatch", ["Paramount Pictures"], ["Ivan Reitman", "Michael Berk", "Douglas Schwartz", "Gregory J. Bonann", "Beau Flynn"], ""],
["2017", "Fifty Shades Darker", ["Universal Pictures"], ["Michael De Luca", "E. L. James","Dana Brunetti", "Marcus Viscidi"], ""],
["2017", "The Mummy", ["Universal Pictures"], ["Alex Kurtzman", "Chris Morgan", "Sean Daniel", "Sarah Bradshaw"], ""],
["2017", "Transformers: The Last Knight", ["Paramount Pictures"], ["Don Murphy", "Tom DeSanto", "Lorenzo di Bonaventura","Ian Bryce"], ""],
["2018", "Holmes & Watson", ["Columbia Pictures"], ["Will Ferrell", "Adam McKay", "Jimmy Miller", "Clayton Townsend"], "yes"],
["2018", "Gotti", ["Vertical Entertainment"], ["Randall Emmett", "Marc Fiore", "Michael Froch","George Furla"], ""],
["2018", "The Happytime Murders", ["STX"], ["Ben Falcone", "Jeffrey Hayes", "Brian Henson", "Melissa McCarthy"], ""],
["2018", "Robin Hood", ["Summit Entertainment"], ["Jennifer Davisson","Leonardo DiCaprio"], ""],
["2018", "Winchester", ["Lionsgate"], ["Tim McGahan","Brett Tomberlin"], ""],
["2019", "Cats", ["Universal Pictures"], ["Debra Hayward", "Tim Bevan", "Eric Fellner", "Tom Hooper"], "yes"],
["2019", "The Fanatic", ["Quiver Distribution"], ["Daniel Grodnik", "Oscar Generale", "Bill Kenwright"], ""],
["2019", "The Haunting of Sharon Tate", ["Saban Films"], ["Lucas Jarach", "Daniel Farrands", "Eric Brenner"], ""],
["2019", "A Madea Family Funeral", ["Lionsgate"], ["Ozzie Areu", "Will Areu", "Mark E. Swinton"], ""],
["2019", "Rambo: Last Blood", ["Lionsgate"], ["Avi Lerner", "Kevin King Templeton", "Yariv Lerner", "Les Weldon"], ""]
]



//Array para armazenar todos os produtores ganhadores
console.log(filmes);

//Array para armazenar todos os filmes ganhadores
var filmesGanhadores = []
//For para ganhar em um array todos os filmes que ja ganharam um premio
for (let i = 0; i < filmes.length; i++) {
    if(filmes[i][4] == "yes"){
        filmesGanhadores.push(filmes[i])
    }    
}

//Array para guardar todos os produtores já premiados
var produtoresPremiados = []
//For para varrer o array de filmes premiados e guardar todos os produtores que já foram premiados
for (let i = 0; i < filmesGanhadores.length; i++) {
    for (let j = 0; j < filmesGanhadores[i][3].length; j++) {
        produtoresPremiados.push(filmesGanhadores[i][3][j])       
    }    
}

while (0) {

    for (let j = 0; j < filmesGanhadores[i][3].length; j++) {
        produtoresPremiados.push(filmesGanhadores[i][3][j])       
    } 
}
 
var produtor = {  
    "produtores":[ { 
        "producer" : "Producer 1",        
        "anoganhou": 2008 }]
 }

produtor.push(
    produtor.produtores.producer = "Joao",
    produtor.produtores.anoganhou = 2015
)

var teste = { 

    "min":[ { 
        "producer" : "Producer 1", 
        "interval" : 1, 
        "previousWin": 2008, 
        "followingWin": 2009 },
        
        {"producer" : "Producer 2", 
        "interval" : 1, 
        "previousWin": 2010, 
        "followingWin": 2011 } ],
    
    "max":[ { 
        "producer" : "Producer 1", 
        "interval" : 99, 
        "previousWin": 1900, 
        "followingWin": 1999 },{ 

        "producer" : "Producer 2", 
        "interval" : 99, 
        "previousWin": 2000, 
        "followingWin": 2099 } ] }

    
        teste.max.push(
            { 
                "producer" : "asdad", 
                "interval" : 95, 
                "previousWin": 190550, 
                "followingWin": 1995559 }
        )

    console.log(produtor);



/*console.log("filmes ganhadores");
console.log(filmesGanhadores);

console.log("produtores: " + filmesGanhadores[7][3]);

console.log(filmesGanhadores[7][3].length);*/

/*console.log("Produtores Premiados");
console.log(produtoresPremiados);*/